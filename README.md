# SVG icon performance comparison

## What?

A small suite of scenarios to test browser performance for loading vector icons.

## Why?

I was intrigued to see how big the differences between the various ways of embedding SVG in the page were.
I also wanted to find the optimal solution for icons in my web-applications (in terms of developer/designer happiness, performance and, to a lesser extent, browser compatibility).

## How?

### Running the server

The server can be run either in HTTP/1.1 or in HTTP/2 mode. This is controlled by the `HTTP2` environment variable.

To run the server `npm start`. 
To run in HTTP/2 mode: `export HTTP2=true; npm start`

The server will start on `localhost:3000` (either `http://` or `https://` depending on which protocol is selected). 
The various scenarios are available at the path that corresponds to their directory name, for example: 
`https://localhost:3000/1-font/`
