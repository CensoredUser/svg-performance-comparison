/* 
 * Extracts total bytes transferred and total time to load page from a HAR file
 */
const fs = require('fs')
const path = require('path')

process.argv.forEach((filePath, i) => {
    if (i < 2) return; // Ignore node and script name
    console.log(`Generating report for: ${filePath}`);

    const harFile = JSON.parse(fs.readFileSync(__dirname + '/' + filePath)).log; // Hack to quickly load JSON synchronously.
    const totalTransferredBytes = harFile.entries.map(entry => entry.response._transferSize).reduce((a,v) => a+v);
    const startTime = new Date(harFile.entries[0].startedDateTime).getTime();
    const lastRequestTime = harFile.entries.map(entry => new Date(entry.startedDateTime).getTime() + entry.time).sort((a,b) => b-a)[0];
    const totalTimeMs = lastRequestTime - startTime;

    console.log(`Results for: ${filePath}`);
    console.log(`Total bytes transferred: ${totalTransferredBytes}`);    
    console.log(`Total time to load: ${totalTimeMs}ms (${totalTimeMs/1000}s)`);    

    fs.appendFileSync('results.txt', `${filePath.split('/').join(' ')} ${totalTransferredBytes} ${totalTimeMs} \n`)
});