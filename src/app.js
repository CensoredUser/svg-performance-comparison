var compression = require('compression')
var express = require('express')
var fs = require('fs')
var spdy = require('spdy')

var app = express()
app.use(compression())

const HTTP2 = process.env.HTTP2 || false;
const SCENARIOS = 'scenarios'
const SSL_OPTS = {
    key: fs.readFileSync(__dirname + '/keys/server.key'),
    cert: fs.readFileSync(__dirname + '/keys/server.crt')
};

app.use(express.static(__dirname + '/' + SCENARIOS))

if (HTTP2) {
    spdy.createServer(SSL_OPTS, app)
    .listen(3000, (error) => {
        if (error) {
        console.error(error)
        return process.exit(1)
        } else {
        console.log('Listening on port: 3000')
        }
    })
} else {
    app.listen(3000, function () {
        console.log('Example app listening on port 3000!')
    })
}